# 悦来云

#### 介绍
目标：一切以业务为中心的持续规模化创新，让业务所要求的哪些变化能随时上线。

悦来云平台采用领域设计思维（DDD），在其上构建业务能力SAAS平台，持续不断迭代演进。基于[[go-micro]](https://github.com/micro/go-micro)

微服务框架开发的插件化架构。

#### 架构图
功能架构（平台化）:

<img width="800" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/%E5%8A%9F%E8%83%BD%E6%9E%B6%E6%9E%84%E5%B9%B3%E5%8F%B0%E5%8C%96.png" />

微服务架构：

<img width="800" src="https://gitlab.com/cgkei23/myblog/raw/master/source/operat-arch.png" />

逻辑架构：

<img width="800" src="https://gitlab.com/cgkei23/myblog/raw/master/source/logical-arch.png" />

数据统计架构：

<img width="800" src="https://gitlab.com/cgkei23/myblog/raw/master/source/stat-arch.png" />

#### 应用
1. 悦来云平台

<img width="500" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/yue-cloud-login.jpg" />

<img width="800" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/yue-cloud-index.jpg" />

2. 用户中心

<img width="500" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/yue-user-center.jpg" />

<img width="500" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/yue-user-list.jpg" />

<img width="500" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/yue-user-role.jpg" />

3. 配置中心

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/config-sms-info.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/config-sms-sign.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/config-sms-list.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/config-app-config.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/config-pay-info.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/config-pay-list.jpg" />

4. 任务中心

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/task-info.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/task-jiangli-rule.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/task-list.jpg" />

<img width="400" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/task-rule-list.jpg" />

5. 财务中心

<img width="500" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/finance-recharge.jpg" />

6. 悦来商城

<img width="500" src="https://gitlab.com/cgkei23/myblog/-/raw/master/source/yue-shop.png" />


#### 服务器环境
##### 软件依赖
1. centos：  7.8.2003
2. docker：  19.03.6
3. php环境：  7.3.0
4. mysql:    8.0.0
5. redis:    5.0.5
6. nginx:    1.18.0
7. node_exporter: 1.0.0
##### docker 镜像
1. SVN：subversion-edge:latest
2. nexus：sonatype/nexus3:latest
3. prometheus: prom/prometheus:latest
4. grafana: grafana/grafana:latest
5. cavdisor: google/cadvisor:latest

ps:详情参考技术文档: [TAPD](https://www.tapd.cn/32266077/markdown_wikis/show/#1132266077001000197)


#### 服务
1. [JWT认证服务](https://www.tapd.cn/32266077/markdown_wikis/show/#1132266077001000208)
2. [Cashin多租户服务](https://casbin.org/docs/zh-CN/rbac)
3. [API文档自动化服务] eolinker+swagger
4. [短信服务]
5. [七牛云存储服务]
6. [DataV]

#### 架构roadmap
- 统一权限认证
 - [ ] [悦来云多域体系]
 - [ ] [悦来云多租户体系]
 - [x] [api-gateway（伪）开发]
 - [x] [用户中心cashbin权限体系开发]
 - [x] [悦来云JWT体系开发]
- 监控平台
 - [ ] [自动告警体系]
 - [x] [grafana](http://monitor.scyuelai.com/) 搭建 
- nexus
 - [x] [docker镜像仓库](http://repo.scyuelai.com/) 搭建 
- jenkins
 - [x] [CI/CD]
 - [x] [jenkins持续集成平台](http://jenkins.scyuelai.com/) 搭建 
- gitlab
 - [x] [webhook]
 - [x] [gitlab代码仓库](http://gitlab.scyuelai.com/) 搭建

#### 项目roadmap
- 悦来商城
 *  商城单点登录 开发周期：2020-10-19 ～ 2020-11-25
    - [ ] 商城单点登录
 *  商城产地直发商品 开发周期：2020-11-09 ～ 2020-11-11
    - [x] 当购买发货商品时，订单确认页新增收货地址
    - [x] 订单详情页新增显示收货地址和备注信息
    - [x] 新增商品字段“是否发货”
    - [x] 新增订单列表筛选“是否发货商品”
    - [x] 新增订单详情显示收货地址、备注信息 
 *  商品补贴需求 开发周期：2020-10-19 ～ 2020-11-09
    - [x] 新增平台补贴商品标签显示及订单确认时的价格判定
    - [x] 优化支付结果页跳转
    - [x] 优化提现流程并加入支付宝转账接口
    - [x] 新增商城数据统计看板
    - [x] 新增商品补贴活动

- 挺好生活手机应用
 * IOS V1.18.0.0 开发周期：2020-10-19 ～ 2020-11-23
    - [ ] 首页动态化布局
    - [ ] 启动页
    - [ ] 区域定位
    - [ ] 修改手机号
    - [ ] 隐私政策、用户协议
    - [ ] 关于我们

- 配置中心
 * 2期 开发周期：2020-10-27 ～ 2020-11-26
    - [ ] 区域管理的内容设置
    - [ ] 挺好生活首页设置内容
    - [ ] 关联小程序的设置内容
    - [ ] 关于我们的设置内容

- 用户中心
 *五期 开发周期：2020-11-09 ～ 2020-11-25
    - [ ] 数据缓存机制
    - [ ] 统一登录(能够统一进行相关的登录，能够直接登录相关的系统)
    - [ ] 协助挺好生活修改相关内容的接口(签名、生日的修改会同步至用户中心)
    - [ ] 列表排序(时间的倒序)
    - [ ] 自定义头部常用应用(可以自己对于头部的内容进行自定义，添加一些自己常用的网站)
    - [ ] 应用分类，可以给应用有相关的分类，在页面展示中，需要根据分类来进行展示
    - [ ] 登录页面的忘记密码的逻辑完善，以及个人中心中更改手机号和邮箱的逻辑完善

- 任务中心 开发周期：2020-11-09 ～ 2020-11-24
- [ ] 邀请有奖
- [ ] 早起打卡
- [ ] 签到

- 2020.11.19~2020.11.25
- [x] 完成swagger+gitlab+eolinker的自动化API管理体系

- 2020.11.01~2020.11.12
- [x] 绘制架构图
- [x] 分离网关到独立仓库
- [x] 实现consul作为**配置中心**，基于go-config实现动态更新配置
- [x] 新增micro网关插件实现**JWT认证**功能
  - [x] 新增User微服务，api类型微服务作为控制层，srv类型微服务为模型层通过gorm访问**DB**
  - [x] 实现模型层接口：用户查询
  - [x] 实现控制层接口：用户登录，用户查询。
  - [x] 实现cashbinPolicy。



